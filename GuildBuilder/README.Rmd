---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
```
# GuildBuilder

<!-- badges: start -->
<!-- badges: end -->

GuildBuilder is a stability based network inference aggregation method from whole metagenome sequencing data, which aims at fostering reproducibility and precision.

## Installation

You can install GuildBuilder from [CRAN](https://CRAN.R-project.org) with:

```{r install, eval=FALSE}
remotes::install_gitlab(repo = "metagenopolis/GTguildes", 
                        subdir="GuildBuilder", 
                        host = "forgemia.inra.fr", 
                        auth_token ="gGx45sZaVeRSZh41M3PY")
```


## Liver data set

This package includes a data set "liver", which contains 

- Abundances (semi-discrete) and metadata for a trial on healthy and liver cirrhosis patients from  Qin et. al 2014 (see \link[https://pubmed.ncbi.nlm.nih.gov/25079328/]{the arrticle here}).
- A taxonomy table.
- Network inferences with 6 methods for a minimal species prevalence of 90% and 50%.

```{r}
library(GuildBuilder)
## basic example code
data("liver")
str(liver, max.level=1)
```
