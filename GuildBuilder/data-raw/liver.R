## code to prepare `liver` dataset goes here
counts.file<-"/projects/shrcan/poc_ba_10.4/data_11/genes_1/mgs_1/PRJEB6337_vs_hs_10_4_igc2_trim80_smart_shared_reads_17112020.fpkm.mgs.100.tsv"
meta.file<-"/projects/public_projects/PRJEB6337/analysis/metadata/PRJEB6337_QinN_2014_LC_237_CHN.xlsx"
save.path<- "/projects/public_projects/PRJEB6337/"
#inferences on all population, and adjusted on status covariate
#(thus requireing SRA filtering on counts during preprocessing)
inf_prev50<-readRDS("/projects/public_projects/PRJEB6337/analysis/guildes/raw_allpop_prev50.rds" )
inf_prev90<-readRDS("/projects/public_projects/PRJEB6337/analysis/guildes/raw_allpop_prev90.rds" )
taxonomy<-read.delim("/projects/biodatabank/catalogue/hs_10_4_igc2/mgs/20190301/taxonomy/20200403/taxo_hs_10.4_1990_MSP_freeze3_20190301_gtdb_corr_20200403.tsv")
counts<-as.matrix(read.delim(counts.file))
meta<- suppressWarnings(read_excel(meta.file,  sheet = "metadata"))
liver <- list(
  abundances=counts,
  metadata=meta,
  infer_prev50=inf_prev50,
  infer_prev90=inf_prev90,
  taxonomy=taxonomy
)

usethis::use_data(liver,overwrite = TRUE)


# inf_prev30<-readRDS("/projects/public_projects/PRJEB6337/analysis/guildes/raw_allpop_covar_prev30.rds" )
# names(inf_prev30)<-c("PLNnetwork","gCoda","SpiecEasi","SPRING", "Magma","EMtree")
# great<-do.call(rbind,inf_prev30$gCoda$output$merge) %>%  as_tibble() %>%
#   rename(Prob=freqs, Penalty=pen)
# inf_prev30$gCoda$output<-great
# saveRDS(inf_prev30,"/projects/public_projects/PRJEB6337/analysis/guildes/raw_allpop_covar_prev30.rds" )



# data("liver")
# abund<-liver$abundances
# meta<-liver$metadata
# counts_from_table<-get_count_table(abund.table=abund, prev.min=0.5, verbatim=FALSE,sample_id = meta$SRA)$data
# some_inferences<-all_inferences(counts_from_table, rep.num=100,edge_thresh = 0.9,n.levels = 100,offset = TRUE,covar=NULL,
#                                 cores=25)
# saveRDS(some_inferences,"/projects/public_projects/PRJEB6337/analysis/guildes/raw_allpop_prev50.rds")
#
# counts_from_table<-get_count_table(abund.table=abund, prev.min=0.9, verbatim=FALSE,sample_id = meta$SRA)$data
# some_inferences<-all_inferences(counts_from_table, rep.num=100,edge_thresh = 0.9,n.levels = 100,offset = TRUE,covar=NULL,
#                                 cores=25)
# saveRDS(some_inferences,"/projects/public_projects/PRJEB6337/analysis/guildes/raw_allpop_prev90.rds")
