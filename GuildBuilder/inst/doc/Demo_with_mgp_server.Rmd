---
title: "Demo with mgp server"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Demo with mgp server}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---
  
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(GuildBuilder)
library(RColorBrewer)
library(xlsx)
library(dplyr)
library(ggplot2)
library(kableExtra)

options(dplyr.summarise.inform = FALSE)
```


# Pré-traitement

Le pré-traitement des données commence avec  la sélection des bons chemins d'accès au projet, stockés dans l'excel "Projects_info.xlsx". Il faut choisir le code (ou nom) du projet pour lequel on veut conduire l'inférence, et récuprérer les chemins. 

Cette démo porte sur le projet PRJEB6337 ("qin nan").

```{r read_info}
file<-system.file("extdata","Projects_info.xlsx", package = "GuildBuilder")
Projects_info<-read.xlsx(file, 1)

# set project name
chosen_code<-"PRJEB6337"

# get file paths from Projects_info
current_project<-Projects_info %>% filter(code==chosen_code)
abund.path<-as.character(current_project$abundance)
meta.file.name<-as.character(current_project$metadata)
save.path<-as.character(current_project$save_results)

#read meta
meta<- suppressWarnings(read.xlsx(meta.file.name, 2)) %>% as_tibble()
```

Ensuite, la fonction `get_count_table()` donne la table de comptage des MSP du projet pour un seuil de prévalence donné ainsi que les prévalences des espèces. Le rôle du seuil de prévalence est d'éviter la présence d'espèces trop peu présentes pour être correctement représentées dans le réseau. Dans la littérature portant sur les réseaux en métagénomique, la valeur de 30% revient souvent. L'avantage d'un seuil élevé est surtout de raccourcir les temps de calculs. Notamment cette vignette nécessite des temps de calcul très faibles, et par conséquent afin d'exposer le fonctionnement du package on impose un filtre stricte de prévalence supérieure à 90%.

Le paramètre sample_id doit donner les  échantillons "propres", ce sont eux qui sont gardés pour la suite des analyses. Par exemple pour le projet PRJEB6337, sample_id est la variable "SRA" des meta-données (des échantillons séquencés peuvent ne correspondre à aucune méta-donnée, et inversement). 

```{r preprocessing}
prev.min<-0.9
counts_and_species<-get_count_table(abund.path=abund.path,
                                    sample_id=meta$SRA, 
                                    prev.min=prev.min)

counts<-counts_and_species$data
species<-colnames(counts)
head(counts_and_species$prevalences)
```


# Inférence des fréquences de sélection

L'inférence des fréquences se déroule en deux étapes :

1. Estimation des chemins de régularisation de toutes les méthodes,
2. Adaptation de la stabilité moyenne de l'ensemble des méthodes au niveau voulu.

Sur le serveur MGP, les estimations peuvent être sauvegardées dans le sous-dossier "analysis/guildes" du projet.

## Estimation avec chaque méthode

L'étape 1 est réalisée par la fonction `all_inferences()`. Les paramètres à régler sont des paramètres de modélisation (covar et offset), de ré-échantillonnage (`rep.num`, `n.levels`, `cores`) et de détection des arêtes (`edge_thresh`). Le paramètre `seed` (non utilisé ici) est également disponible pour faire varier les résultats de la méthode SPRING.

Pour cette démo on ajuste la covariable de statut (malade "LC" ou sain "H"), avec estimation d'offset car on part de la table non raréfiée. Par défaut le nombre de rééchantillons est fixé à `rep.num=100`, mais ici par soucis de temps de calcul `rep.num=30`. Ce paramètre augmente la précision du calcule des fréquences de sélection.

De même, pour les inférences il est très bénéfique (et très conseillé) d'utiliser le calcul parallélisé avec le paramètre `cores` (par exemple `cores=20`). Par défaut la valeur est 1 afin de convenir aux utilisateurs windows, et ici `cores=2` pour respecter les règles de construction d'un package. 

```{r inference}
# run inference and save output in project file
rep.num=20; n.levels=100; edge_thresh=0.9; cores=2
inference_collection=all_inferences(data=counts,edge_thresh=edge_thresh,
                                    rep.num = rep.num,n.levels = n.levels, cores = cores,
                                    offset=TRUE, covar=meta$status)
save.name<-"raw_allpop_covar" #raw: no downsizing, allpop: no subsetting, covar: with covariate
saveRDS(inference_collection,paste0(save.path,"analysis/guildes/", save.name,"_prev",
                                    prev.min*100,".rds" ))
```

## Ajustement de la stabilité moyenne

La deuxième étape vise l'ajustement de la stabilité de chaque méthode pour conserver un niveau comparable de précision. Des simulations non présentées ici montrent que le nombre d'arêtes sélectionnées (ou densité) est une quantité intermédiaire intéressante pour retrouver des niveaux similaires de précision sur des données empiriques. Par conséquent, la fonction `adapt_mean_stability()` trouve la densité correpsondante au niveau moyen de stabilité demandé sur l'ensemble des méthodes. Les vecteurs de fréquences conservés pour chaque méthode sont ensuite ceux qui correspondent au mieu à la densité trouvée.

Le paramètre important de `adapt_mean_stability()` est `mean.stability`, dont on peut voir l'effet sur le nombre d'arêtes trouvées avec la sortie graphique si `plot=TRUE`. Des simulations non présentées ici montrent qu'augmenter le niveau de stabilité moyen permet d'augmenter le niveau de reproductibilité des arêtes trouvées. Nous recommandons un niveau de stbailité moyen de 90%.  

Pour avoir suffisemment d'arêtes, on utilise une inférence déjà sauvegardée, pour un seuil de prévalence de 50%.

```{r adapt_mean_stab}
#Adapt mean stability to get edges selection frequencies
data_prev50<-get_count_table(abund.path=abund.path,
                                  sample_id=meta$SRA, 
                                  prev.min=0.5)
read.name<-"raw_allpop_prev50"
inference_collection<-readRDS(paste0(save.path,"analysis/guildes/", read.name,".rds" ))
adapted<-adapt_mean_stability(inference_collection,mean.stability=0.9, plot=TRUE)
final_frequences<-adapted$freqs
```

```{r,eval=FALSE}
head(final_frequences) 

```


```{r,echo=FALSE}
head(final_frequences) %>% kbl() %>%
  kable_styling()

```

Les fréquences sont ensuite agrégées avec `compute_aggreg_measures()` qui propose plusieurs scores d'agrgation (moyenne, norme 2, inverse variance weighting, et le nombre de fréquences élevées).

```{r, eval=FALSE}
aggreg_data=compute_aggreg_measures(final_frequences)
head(aggreg_data)  
```

```{r, echo=FALSE}
aggreg_data=compute_aggreg_measures(final_frequences)
head(aggreg_data) %>%  kbl() %>%
  kable_styling()
```

Il est possible d'observer les stabilités finales obtenues pour chaque méthode:

```{r, eval=FALSE}
adapted$stab_data 
```
```{r, echo=FALSE}
adapted$stab_data %>% kbl() %>%
  kable_styling()
```

# Guildes

Une fois le réseau inféré, on peut l'utiliser pour identifier des guildes microbiennes.

L'identification de guildes se fait pour le moment avec une approche par modèle stochastique à blocs latent (SBM), qui a la souplesse d'identifier des groupes avec des motifs de connectivité différents. C'est à dire qu'SBM trouve à la fois des communautés (forte connectivité intra groupe), et des groupes d'espèces peu liées entre elles mais reliées de manière similaire au reste du réseau. Cela permet d'identifier des structures coeur-périphérie par exemple, ou le coeur serait dans un groupe et la périphérie dans un autre. Par conséquent, les groupes trouvés par SBM ne sont pas tous des guildes microbiennes.


la fonction `clustering_SBM()` donne le clustering SBM optimal pour les scores au-dessus du seuil threshold, et renvoie :

- `nb_groups` : nombre de guildes trouvées  
- `graph` : sortie graphique (réseau)
- `species_groups` : les espèces présentes dans chaque groupe (identifiant msp, nom de l'espèce et son phyllum).
- `groups` : le groupe associé à chaque espèce (vecteur)

Retrouvons tout d'abord les espèces sélectionnées:
```{r}
species<-colnames(data_prev50$data)
```

```{r}
taxo <-read.delim(as.character(Projects_info$metadata[Projects_info$name=="taxonomy"]))
selected_taxo=left_join(data.frame(msp_name=species),taxo, by="msp_name")

clusters=clustering_SBM(aggreg_data$norm2,freq.thresh=0.9,selected_taxo=selected_taxo,force.nb.groups = 2)
clusters$graph
```

```{r,eval=FALSE}
head(clusters$species_groups[[2]])
```


```{r, echo=FALSE}
head(clusters$species_groups[[2]]) %>% kbl() %>%  kable_styling()
```

# Visualiser

Il est possible de visualiser le réseau obtenu, pondéré par les fréquences (épaisseur des arêtes proportionnelle aux fréquences) ou bien en seuillant les fréquences aggrégées (réseau binaire). 
Différentes couleurs d'arêtes peuvent être spécifiées pour repérer les signes des corrélations partielles. De même les couleurs de noeuds permettent de facilement repérer les groupes identifiés par SBM.

Dans l'exemple ci-dessous, on représente le réseau des arêtes ayant une fréquence de sélection supérieure à 90%, et les noeuds selon les groupes identifiés (pour un seuil de fréquences à 80%). Fixer le paramètre `btw_rank` à 1 permet de ne pas mettre en valeur les neuds à fort score de btweenness.

```{r, fig.width=6}
library(RColorBrewer)
EMtree::draw_network(ToSym(1*(aggreg_data$norm2>0.9)),  
                     node_groups = clusters$groups,
                     pal_edges =  c("darkorange","#31374f"),
                     pal_nodes = brewer.pal(3, "Dark2"),nodes_size = c(2,2),
                     layout="nicely", btw_rank = 1)$G
```


`GuildBuilder` fournit également la fonction `ggimage()`, qui permet de représenter une heatmap en `ggplot` tout en controllant facilement l'ordre des lignes (et des colonnes séparément pour les matrices rectangulaires ou non symmétriques). Par exemple, pour voir la répartition de la norme 2 au sein des deux groupes identifiés :

```{r}
ggimage(ToSym(aggreg_data$norm2), order = order(clusters$groups),
    no.names=TRUE)

```



# Corrélations partielles

Les corrélations partielles sont une mesure de la force et du signe de l'intéraction directe entre deux espèces. Elles sont particulièrement utiles lors d'une comparaison de réseaux, ou lorsque l'on s'intéresse précisément à un petit nombre d'espèces. 

Le calcul des corrélations partielles repose sur deux éléments : 

- Un estimteur des arêtes du réseau,
- Un estimateur de la matrice de variance-covariance.

En exploitant les modèles graphiques gaussiens, le package `ggm` nous donne les estimations des corrélations partielles.

Pour estimer la matrice de variance-covariance,  une matrice de corrélation latente est estimée à partir de données observées, grâce au modèle de copules gaussiennes latentes. C'est la stratégie de SPRING, implémentée dans le package `mixedCCA`.  

Ce calcul n'est pas inclu dans le package `GuildBuilder` à dessein, afin de laisser l'utilisateur libre du choix de la méthode pour l'estimation de Sigma. Un exemple est retranscrit ici à visée pédagogique.

```{r}
# retrouver les comptages pour la prévalence de 30%
 
qdat <- SPRING::mclr(data_prev50$data)

Sigma <- mixedCCA::estimateR(qdat, type = "trunc", method = "approx",
                             tol = 1e-6, verbose = FALSE)$R
```

Le vecteur `Parr_Corr` obtenu par la fonction `get_par_corr()` de `GuildBuilder`, contient toutes les corrélations partielles calculées à partir de Sigma et du réseau obtenu en seuillant les fréquences agrégées à `freq.thresh=0.9`. L'utilité de ce seuil est d'identifier l'ensemble des arêtes pour lesquelles on veut calculer les corrélations partielles, sachant que seules les arêtes réellement présentes seront correctement estimées (ce qui plaide pour une précision maximale plutôt qu'un compromis précision/puissance).

```{r}
n<-nrow(data_prev50$counts)
freqs<-aggreg_data$norm2
Parr_Corr<-get_par_corr(freqs,Sigma,n,freq.thresh=0.9)

```


```{r, echo=FALSE, fig.width=6}
Parr_Corr %>% filter(abs(corp_ggm)>0.001) %>%  ggplot(aes(corp_ggm))+
  geom_density(alpha=0.1, fill="cornflowerblue")+theme_light()+
  labs(title="Distribution des corrélations partielles \nnon nulles (abs>1e-3) estimées")
```

On peut visualiser le signe des corrélations partielles sur le graphe, en renseignant le paramètre `edge_groups` :

```{r}
EMtree::draw_network(ToSym(1*(aggreg_data$norm2>0.9)), 
                     node_groups = clusters$groups,  pal_nodes = brewer.pal(3, "Dark2"),nodes_size = c(2,2),
                     edge_groups = sign(ToSym(Parr_Corr$corp_ggm)),
                     pal_edges =  c("darkorange","#31374f"),
                     layout="nicely", btw_rank = 1)$G
```


# Performances sur données simulées

`GuildBuilder` permet de faire des expériences sur données simulées. Pour ce faire, le package simule des données avec la fonction `new_synth_data()`, puis évalue les performances des inférences grâce à la fonction `evalQuali()`.


La simulation des données se fait en suivant  le protocole de la méthode SPRING, détaillé dans Yoon et al. (2019). Les données, simulées de manière jointe, vont s'ajuster au mieux aux distributions marginales observées dans un jeu de données réelles. En outre, cette procédure reproduit les proportions de zéro observées (par espèce uniquement, pas par individu).

En reprenant l'exemple avec les données du serveur pour une prévalence de 90%:

```{r}
count_data<-counts_and_species$data
simulation<-new_synth_data(count_data, graph_type="cluster",n=100, plot=TRUE)
str(simulation, max.level=1)
summary(c(count_data))
summary(c(simulation$counts))
```

Le résumé des deux jeux de données sont  proches.

Viennent ensuite les inférences, ici avec l'ensemble des méthodes disponibles:

```{r, include=FALSE}
some_inferences<-all_inferences(simulation$counts ,edge_thresh=0.9,n.levels=50,
                                rep.num=6, cores=2) # en 2 fois car abandon face au bug d'affichage 
```

```{r, eval=FALSE}
some_inferences<-all_inferences(simulation$counts ,edge_thresh=0.9,n.levels=50,
                                rep.num=6, cores=2)
```

Enfin, on évalue les performances des méthodes avec `evalQuali`. La valeur retournée est un tableau rassemblant la précision, le rappel, la stabilité, la méthode en question et le niveau de pénalité lambda. 

```{r,warning=FALSE, fig.width=7}
Eval_inference<-evalQuali(some_inferences,simulation$G)
head(Eval_inference)
Eval_inference %>% ggplot(aes(TPR, PPV, color=method))+geom_point()+geom_line()+theme_light()#
```

