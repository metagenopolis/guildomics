#' Liver cirrhosis data set
#'
#' This data set contains abundance data and meta data from Qin et. al 2014 (see \link[https://pubmed.ncbi.nlm.nih.gov/25079328/]{the arrticle here}).
#' The liver data set also contains two inferences already computed with all 6 inference methods, for two different prevalence levels (infer_prev50 and infor_prev90).
#' A taxonomy table of msp is also included for practicality, internally computed at MetaGenoPolis.
#'
#' @name liver
#' @docType data
#' @author Raphaelle Momal \email{raphaelle.momal@@inrae.fr}
#' @source \url{google.com}{unlien}
#' @format list
#' @keywords data
"liver"
