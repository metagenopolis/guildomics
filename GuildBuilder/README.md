
<!-- README.md is generated from README.Rmd. Please edit that file -->

# GuildBuilder

<!-- badges: start -->

<!-- badges: end -->

GuildBuilder is a stability based network inference aggregation method
from whole metagenome sequencing data, which aims at fostering
reproducibility and precision.

## Installation

You can install the released version of GuildBuilder from
[CRAN](https://CRAN.R-project.org) with:

``` r
remotes::install_gitlab(repo = "metagenopolis/GTguildes", 
                        subdir="GuildBuilder", 
                        host = "forgemia.inra.fr", 
                        auth_token ="gGx45sZaVeRSZh41M3PY")
```

## Liver data set

This package includes a data set “liver”, which contains

  - Abundances (semi-discrete) and metadata for a trial on healthy and
    liver cirrhosis patients from Qin et. al 2014 (see ).
  - A taxonomy table.
  - Network inferences with 6 methods for a minimal species prevalence
    of 90% and 50%.

<!-- end list -->

``` r
library(GuildBuilder)
## basic example code
data("liver")
str(liver, max.level=1)
#> List of 5
#>  $ abundances  : chr [1:1990, 1:238] "msp_0001" "msp_0002" "msp_0003" "msp_0004" ...
#>   ..- attr(*, "dimnames")=List of 2
#>  $ metadata    : tibble [222 × 27] (S3: tbl_df/tbl/data.frame)
#>  $ infer_prev50:List of 6
#>  $ infer_prev90:List of 6
#>  $ taxonomy    :'data.frame':    1990 obs. of  21 variables:
```
